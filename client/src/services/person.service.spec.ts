import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PersonService } from './person.service';
import { mockPersons } from 'src/mocks/types/mock-persons';
import { Person } from 'src/types/types';

describe('PersonService', () => {
  let personService: PersonService;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PersonService],
    });
    backend = TestBed.get(HttpTestingController);
    personService = TestBed.get(PersonService);
  });
  afterEach(() => {
    backend.verify();
  });
  it('should be created', () => {
    const service: PersonService = TestBed.get(PersonService);
    expect(service).toBeTruthy();
  });

  it('should be able to fetch all persons', () => {
    personService.getPersons().subscribe(personList => {
      expect(personList.personList).toEqual(mockPersons);
    });
    const req = backend.expectOne('/app/persons');
    expect(req.request.method).toEqual('GET');
    req.flush(mockPersons);
  });
  it('should be able to get a person by id', () => {
    personService.getPersonById(mockPersons[3].id).subscribe(personObject => {
      expect(personObject.person).toEqual(mockPersons[3]);
    });
    const req = backend.expectOne('/app/persons/' + mockPersons[3].id);
    expect(req.request.method).toEqual('GET');
    req.flush(mockPersons);
  });
  it('should be able to post a person', () => {
    const newPerson: Person = {
      lastName: 'mocky',
      profession: 'mocking mocker',
      bio: 'mock mock mock',
      url: 'mock.mock',
      imageUri: 'mockPersonImageUri.mock',
      name: 'mock',
      id: 'mockid',
      realName: 'mockrealmock',
    };
    personService.addPerson(newPerson).subscribe(postedPerson => {
      expect(postedPerson.person).toEqual(newPerson);
    });
    const req = backend.expectOne('/app/persons');
    expect(req.request.method).toEqual('POST');
    req.flush(newPerson);
  });
});
