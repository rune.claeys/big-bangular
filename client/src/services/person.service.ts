import { Injectable } from '@angular/core';
import { Person, PersonList, PersonObject } from '../types/types';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class PersonService {
  constructor(private http: HttpClient) {}

  private handleError<T>(operation: string, result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }

  getPersonById(id: string): Observable<PersonObject> {
    return this.http
      .get<PersonObject>('/app/persons/' + id)
      .pipe(catchError(this.handleError<PersonObject>('getPersonById')));
  }

  getPersons(): Observable<PersonList> {
    return this.http.get<PersonList>('/app/persons').pipe(catchError(this.handleError<PersonList>('getPersons')));
  }

  addPerson(person: Person): Observable<PersonObject> {
    return this.http
      .post<PersonObject>('/app/persons', person)
      .pipe(catchError(this.handleError<PersonObject>('addPerson')));
  }

  deletePerson(person: Person): Observable<PersonObject> {
    return this.http
      .delete<PersonObject>('/app/persons/' + person.id)
      .pipe(catchError(this.handleError<PersonObject>('deletePerson')));
  }

  updatePerson(person: Person): Observable<PersonObject> {
    return this.http
      .put<PersonObject>(`/app/persons/`, person)
      .pipe(catchError(this.handleError<PersonObject>('updatePerson')));
  }
}
