import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CharacterListItemComponent } from './character-list-item.component';
import { mockPersons } from 'src/mocks/types/mock-persons';
import { RouterModule } from '@angular/router';
import { Subject } from 'rxjs';
import { Person } from 'src/types/types';
import { PersonService } from 'src/services/person.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_BASE_HREF } from '@angular/common';

describe('CharacterListItemComponent', () => {
  let component: CharacterListItemComponent;
  let fixture: ComponentFixture<CharacterListItemComponent>;
  const mockPersonSubject = new Subject<Person>();
  const mockPersonService = {
    getPerson: jest.fn(() => mockPersonSubject),
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterListItemComponent],
      imports: [RouterModule.forRoot([]), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: PersonService,
          useValue: mockPersonService,
        },
        { provide: APP_BASE_HREF, useValue: '/' },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterListItemComponent);
    component = fixture.componentInstance;
    component.person = mockPersons[0];
    component.isDelete = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render correctly', () => {
    expect(fixture).toMatchSnapshot();
  });
});
