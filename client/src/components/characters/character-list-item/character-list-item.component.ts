import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Person } from 'src/types/types';

@Component({
  selector: 'app-character-list-item',
  templateUrl: './character-list-item.component.html',
  styleUrls: ['./character-list-item.component.scss'],
})
export class CharacterListItemComponent implements OnInit {
  @Input() person: Person;
  @Input() isDelete: boolean;
  @Output() deletePerson = new EventEmitter<Person>();

  onDeletePerson(person: Person): void {
    this.deletePerson.emit(person);
  }

  constructor() {}
  ngOnInit() {}
}
