import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from 'src/types/types';

@Component({
  selector: 'app-add-character',
  templateUrl: './add-character.component.html',
  styleUrls: ['./add-character.component.scss'],
})
export class AddCharacterComponent implements OnInit {
  @Input() isAdd: boolean;
  @Output() addPerson = new EventEmitter<Person>();
  isEdit = true;
  person: Person;

  constructor() {}

  onAddPerson(person: Person) {
    this.addPerson.emit(person);
  }
  ngOnInit() {}
}
