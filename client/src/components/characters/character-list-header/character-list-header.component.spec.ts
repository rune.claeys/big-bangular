import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterListHeaderComponent } from './character-list-header.component';

describe('CharacterListHeaderComponent', () => {
  let component: CharacterListHeaderComponent;
  let fixture: ComponentFixture<CharacterListHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterListHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', () => {
    expect(fixture).toMatchSnapshot();
  });
});
