import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-character-list-header',
  templateUrl: './character-list-header.component.html',
  styleUrls: ['./character-list-header.component.scss'],
})
export class CharacterListHeaderComponent implements OnInit {
  @Input() isAdd: boolean;
  @Input() isDelete: boolean;
  @Output() toggleIsAdd = new EventEmitter();
  @Output() toggleIsDelete = new EventEmitter();
  @Output() refresh = new EventEmitter();

  onToggleIsAdd() {
    this.toggleIsAdd.emit();
  }
  onToggleIsDelete() {
    this.toggleIsDelete.emit();
  }

  onRefresh() {
    this.refresh.emit();
  }

  constructor() {}

  ngOnInit() {}
}
