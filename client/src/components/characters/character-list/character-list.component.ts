import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from 'src/types/types';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss'],
})
export class CharacterListComponent implements OnInit {
  @Input() persons: Person[];
  @Output() addPerson = new EventEmitter<Person>();
  @Output() deletePerson = new EventEmitter<Person>();

  isAdd = false;
  isDelete = false;

  onAddPerson(person: Person): void {
    this.setToggleAddPerson();
    this.addPerson.emit(person);
  }

  onDeletePerson(person: Person): void {
    this.deletePerson.emit(person);
  }

  setToggleAddPerson(): void {
    this.isAdd = !this.isAdd;
  }

  setToggleDeletePerson(): void {
    this.isDelete = !this.isDelete;
  }

  ngOnInit() {}
}
