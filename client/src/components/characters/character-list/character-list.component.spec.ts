import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CharacterListComponent } from './character-list.component';

describe('CharacterListComponent', () => {
  let component: CharacterListComponent;
  let fixture: ComponentFixture<CharacterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterListComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', () => {
    expect(fixture).toMatchSnapshot();
  });

  it('should be able to toggle isEdit', () => {
    component.isAdd = false;
    component.setToggleAddPerson();
    expect(component.isAdd).toBeTruthy();
    component.setToggleAddPerson();
    expect(component.isDelete).toBeFalsy();
  });

  it('should be able to toggle isDelete', () => {
    component.isDelete = false;
    component.setToggleDeletePerson();
    expect(component.isDelete).toBeTruthy();
    component.setToggleDeletePerson();
    expect(component.isDelete).toBeFalsy();
  });
});
