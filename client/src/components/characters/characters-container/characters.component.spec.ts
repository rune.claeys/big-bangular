import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CharactersComponent } from './characters.component';
import { PersonService } from 'src/services/person.service';
import { Subject } from 'rxjs';
import { Person } from 'src/types/types';
import { mockPersons } from 'src/mocks/types/mock-persons';

describe('CharactersComponent', () => {
  let component: CharactersComponent;
  let fixture: ComponentFixture<CharactersComponent>;

  const mockPersonsSubject = new Subject<Person>();
  const mockPersonService = {
    getPersons: jest.fn(() => mockPersonsSubject),
    getPersonById: jest.fn(() => mockPersonsSubject),
    addPerson: jest.fn(() => mockPersonsSubject),
    deletePerson: jest.fn(() => mockPersonsSubject),
    updatePerson: jest.fn(() => mockPersonsSubject),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharactersComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: PersonService,
          useValue: mockPersonService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', () => {
    expect(fixture).toMatchSnapshot();
  });

  it('should be able to call getPersons()', () => {
    component.getPersons();
    expect(mockPersonService.getPersons).toHaveBeenCalled();
  });

  it('should be able to call addPerson()', () => {
    component.setAddPerson(mockPersons[1]);
    expect(mockPersonService.addPerson).toHaveBeenCalled();
  });

  it('should be able to call deletePerson()', () => {
    component.setDeletePerson(mockPersons[1]);
    expect(mockPersonService.deletePerson).toHaveBeenCalled();
  });

  it('should be able to call updatePerson()', () => {
    component.setUpdatePerson(mockPersons[1]);
    expect(mockPersonService.updatePerson).toHaveBeenCalled();
  });
});
