import { Component, OnInit } from '@angular/core';
import { Person } from '../../../types/types';
import { PersonService } from 'src/services/person.service';
@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss'],
})
export class CharactersComponent implements OnInit {
  persons: Person[];

  setAddPerson(person: Person) {
    this.personService.addPerson(person).subscribe(value => {
      this.getPersons();
    });
  }

  setDeletePerson(person: Person) {
    this.personService.deletePerson(person).subscribe(value => {
      this.getPersons();
    });
  }

  setUpdatePerson(person: Person) {
    this.personService.updatePerson(person).subscribe(value => {
      this.getPersons();
    });
  }

  getPersons() {
    this.personService.getPersons().subscribe(persons => {
      this.persons = persons.personList;
    });
  }

  constructor(private personService: PersonService) {}

  ngOnInit() {
    this.getPersons();
  }
}
