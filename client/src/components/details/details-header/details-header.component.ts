import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from 'src/types/types';

@Component({
  selector: 'app-details-header',
  templateUrl: './details-header.component.html',
  styleUrls: ['./details-header.component.scss'],
})
export class DetailsHeaderComponent implements OnInit {
  constructor() {}

  @Input() person: Person;
  @Input() isActive: boolean;
  @Output() toggleIsActive = new EventEmitter();
  @Output() updatePerson = new EventEmitter<Person>();

  onToggleIsActive(): void {
    this.toggleIsActive.emit();
  }

  ngOnInit() {}
}
