import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsComponent } from './details.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subject } from 'rxjs';
import { Person } from 'src/types/types';
import { PersonService } from 'src/services/person.service';
import { mockPersons } from 'src/mocks/types/mock-persons';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  const mockPersonsSubject = new Subject<Person>();
  const mockPersonService = {
    getPersons: jest.fn(() => mockPersonsSubject),
    updatePerson: jest.fn(() => mockPersonsSubject),
    getPersonById: jest.fn(() => mockPersonsSubject),
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [RouterModule.forRoot([]), HttpClientTestingModule],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        {
          provide: PersonService,
          useValue: mockPersonService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', () => {
    expect(fixture).toMatchSnapshot();
  });

  it('should be able to call updatePerson()', () => {
    component.setUpdatePerson(mockPersons[1]);
    expect(mockPersonService.updatePerson).toHaveBeenCalled();
  });

  it('should be able to toggle isActive', () => {
    component.isActive = false;
    component.setIsActive();
    expect(component.isActive).toBeTruthy();
    component.setIsActive();
    expect(component.isActive).toBeFalsy();
  });
});
