import { Component, OnInit } from '@angular/core';
import { Person } from '../../../types/types';
import { ActivatedRoute } from '@angular/router';
import { PersonService } from 'src/services/person.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  constructor(private route: ActivatedRoute, private personService: PersonService) {}

  isActive = false;
  person: Person;

  getPerson(): void {
    this.route.params.subscribe(value => {
      if (value) {
        this.getPersonById(value.id);
      }
    });
  }

  getPersonById(personId: string): void {
    this.personService.getPersonById(personId).subscribe(value => {
      this.person = value.person;
    });
  }

  setUpdatePerson(person: Person): void {
    this.personService.updatePerson(person).subscribe(value => {
      this.getPersonById(person.id);
      this.setIsActive();
    });
  }
  setIsActive() {
    this.isActive = !this.isActive;
  }

  ngOnInit() {
    this.getPerson();
  }
}
