import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsContentComponent } from './person-form.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { mockPersons } from 'src/mocks/types/mock-persons';

describe('DetailsContentComponent', () => {
  let component: DetailsContentComponent;
  let fixture: ComponentFixture<DetailsContentComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsContentComponent],
      imports: [ReactiveFormsModule],
      providers: [{ provide: FormBuilder, useValue: formBuilder }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsContentComponent);
    component = fixture.componentInstance;
    component.person = mockPersons[2];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
