import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Person } from 'src/types/types';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.scss'],
})
export class DetailsContentComponent implements OnInit {
  editPersonForm: FormGroup;
  @Input() person: Person;
  @Input() isActive: boolean;
  @Input() formType: string;
  @Output() updatePerson = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {}

  createForm() {
    this.editPersonForm = this.formBuilder.group(
      this.formType === 'edit'
        ? {
            name: [this.person.name, Validators.required],
            lastName: [this.person.lastName, Validators.required],
            url: this.person.url,
            profession: this.person.profession,
            bio: this.person.bio,
            realName: [this.person.realName, Validators.required],
          }
        : {
            name: ['', Validators.required],
            lastName: ['', Validators.required],
            url: '',
            profession: '',
            bio: '',
            realName: ['', Validators.required],
          }
    );
  }
  get name() {
    return this.editPersonForm.get('name');
  }
  get lastName() {
    return this.editPersonForm.get('lastName');
  }
  get realName() {
    return this.editPersonForm.get('realName');
  }

  onSubmit(personData) {
    Object.keys(this.editPersonForm.controls).forEach(field => {
      const control = this.editPersonForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    if (this.editPersonForm.valid) {
      if (this.formType === 'edit') {
        const id = this.person.id;
        this.person = personData;
        this.person.id = id;
      } else {
        this.person = personData;
      }
      this.updatePerson.emit(this.person);
    }
  }
  ngOnInit() {
    this.createForm();
  }
}
