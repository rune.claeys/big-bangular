import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharactersComponent } from '../components/characters/characters-container/characters.component';
import { DetailsComponent } from 'src/components/details/details-container/details.component';

const routes: Routes = [
  {
    path: '',
    component: CharactersComponent,
    pathMatch: 'full',
  },
  {
    path: 'details/:id',
    component: DetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
