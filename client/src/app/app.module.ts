import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharactersComponent } from '../components/characters/characters-container/characters.component';
import { CharacterListComponent } from '../components/characters/character-list/character-list.component';
import { CharacterListItemComponent } from '../components/characters/character-list-item/character-list-item.component';
import { HeaderComponent } from '../components/shared/header/header.component';
import { DetailsHeaderComponent } from '../components/details/details-header/details-header.component';
import { DetailsContentComponent } from '../components/shared/details-content/person-form.component';
import { DetailsComponent } from '../components/details/details-container/details.component';
import { AddCharacterComponent } from '../components/characters/add-character/add-character.component';
import { CharacterListHeaderComponent } from '../components/characters/character-list-header/character-list-header.component';

@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent,
    DetailsComponent,
    CharacterListComponent,
    CharacterListItemComponent,
    HeaderComponent,
    DetailsHeaderComponent,
    DetailsContentComponent,
    AddCharacterComponent,
    CharacterListHeaderComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
