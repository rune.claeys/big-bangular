export interface Person {
  lastName: string;
  profession: string;
  bio: string;
  url: string;
  imageUri: string;
  name: string;
  id: string;
  realName: string;
}

export interface PersonList {
  personList: Person[];
}
export interface PersonObject {
  person: Person;
}
