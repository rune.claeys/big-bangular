import { AppPage } from './app.po';
import { browser, logging } from 'protractor';
import { mockPersons } from 'src/mocks/types/mock-persons';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Big Bangular');
  });
  it('should show details when list element is clicked on', () => {
    page.showDetails();
    expect(page.getDetailsHeaderTitle()).toBeDefined();
  });

  afterEach(async () => {
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
