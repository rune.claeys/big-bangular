import { browser, by, element } from 'protractor';
import { mockPersons } from 'src/mocks/types/mock-persons';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }

  showDetails() {
    return element(by.css('.character-list-item')).click();
  }

  getDetailsHeaderTitle() {
    return element(by.css('.details-header__title')).getText() as Promise<
      string
    >;
  }
}
